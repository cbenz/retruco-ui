// Retruco-UI -- Web user interface to bring out shared positions from argumented statements
// By: Paula Forteza <paula@retruco.org>
//     Emmanuel Raviart <emmanuel@retruco.org>
//
// Copyright (C) 2016 Paula Forteza & Emmanuel Raviart
// https://git.framasoft.org/retruco/retruco-ui
//
// Retruco-UI is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Retruco-UI is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import "babel-polyfill"

import ReactDOM from "react-dom"
import {Provider} from "react-redux"
import {browserHistory, Router} from "react-router"
import {syncReduxAndRouter} from 'redux-simple-router'

import configureStore from "./store"
import routes from "./routes"


const store = configureStore(window.__INITIAL_STATE__)


syncReduxAndRouter(browserHistory, store)

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      {routes}
    </Router>
  </Provider>,
  document.getElementById("app-mount-node"),
)
