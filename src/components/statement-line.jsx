// Retruco-UI -- Web user interface to bring out shared positions from argumented statements
// By: Paula Forteza <paula@retruco.org>
//     Emmanuel Raviart <emmanuel@retruco.org>
//
// Copyright (C) 2016 Paula Forteza & Emmanuel Raviart
// https://git.framasoft.org/retruco/retruco-ui
//
// Retruco-UI is free statement; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Statement Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Retruco-UI is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {Component, PropTypes} from "react"
import {Link} from "react-router"
import {Arrow, Block} from "rebass"
import {Box, Flex} from "reflexbox"


export default class StatementLine extends Component {
  static defaultProps = {
    link: false,
  }
  static propTypes = {
    link: PropTypes.bool,
    statement: PropTypes.object.isRequired,
  }
  render() {
    const {link, statement} = this.props
    let statementBody =
      statement.type === "PlainStatement" ? (
        <Box
        >
          {statement.name}
        </Box>
      ) : (
        <Box
        >
          TODO: {statement.id} of type {statement.type}
        </Box>
      )
    if (link) statementBody = <Link to={`/statements/${statement.id}`}>{statementBody}</Link>
    const score = statement.ratingCount ? (
      <span>{statement.rating} ({statement.ratingSum} / {statement.ratingCount})</span>
    ) : null
    return (
      <Flex align="center">
        <Box
        >
          <Block
            border={true}
            color="blue"
            px={2}
          >
            <div><Arrow direction="up" /></div>
            {score}
            <div><Arrow direction="down" /></div>
          </Block>
        </Box>
        {statementBody}
      </Flex>
    )
  }
}
