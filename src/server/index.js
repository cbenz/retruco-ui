// Retruco-UI -- Web user interface to bring out shared positions from argumented statements
// By: Paula Forteza <paula@retruco.org>
//     Emmanuel Raviart <emmanuel@retruco.org>
//
// Copyright (C) 2016 Paula Forteza & Emmanuel Raviart
// https://git.framasoft.org/retruco/retruco-ui
//
// Retruco-UI is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Retruco-UI is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import express from "express"
import path from "path"
import favicon from "serve-favicon"

import config from "../config"
import handleRender from "./render"


const app = express()
app.set("trust proxy", config.proxy)

app.use(favicon(path.resolve(__dirname, "../../public/favicon.ico")))
app.use(handleRender)

const host = config.listen.host
const port = config.listen.port || config.port
app.listen(port, host, () => {
  console.log(`Server listening at http://${host}:${port}/`)
})
