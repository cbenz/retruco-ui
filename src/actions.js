// Retruco-UI -- Web user interface to bring out shared positions from argumented statements
// By: Paula Forteza <paula@retruco.org>
//     Emmanuel Raviart <emmanuel@retruco.org>
//
// Copyright (C) 2016 Paula Forteza & Emmanuel Raviart
// https://git.framasoft.org/retruco/retruco-ui
//
// Retruco-UI is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Retruco-UI is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


const FAILURE = "FAILURE"
const REQUEST = "REQUEST"
const SUCCESS = "SUCCESS"


function createRequestTypes(base) {
  const res = {};
  [REQUEST, SUCCESS, FAILURE].forEach(type => res[type] = `${base}_${type}`)
  return res;
}


export const LOAD_STATEMENT = "LOAD_STATEMENT"
export const LOAD_STATEMENTS = "LOAD_STATEMENTS"
export const STATEMENT = createRequestTypes("STATEMENT")
export const STATEMENTS = createRequestTypes("STATEMENTS")


function action(type, payload = {}) {
  return {type, ...payload}
}


export const statement = {
  failure: (id, error) => action(STATEMENT.FAILURE, {id, error}),
  request: id => action(STATEMENT.REQUEST, {id}),
  success: (id, result) => action(STATEMENT.SUCCESS, {id, result}),
}


export const statements = {
  failure: error => action(STATEMENTS.FAILURE, {error}),
  request: () => action(STATEMENTS.REQUEST),
  success: result => action(STATEMENTS.SUCCESS, {result}),
}


export const loadStatement = (id) => action(LOAD_STATEMENT, {id})
export const loadStatements = () => action(LOAD_STATEMENTS)


// import {fetchApiJson} from "./fetchers"
// import {GET_STATEMENT, LIST_STATEMENTS} from "./constants"
// import schemas from "./schemas"


// export function getStatement(id) {
//   return {
//     type: GET_STATEMENT,
//     promise: fetchApiJson(`statements/${id}`, schemas.STATEMENT),
//   }
// }


// export function getStatementIfNeeded(id) {
//   return (dispatch, getState) => {
//     const {statementById} = getState()
//     if (!statementById || !statementById[id]) {
//       return dispatch(getStatement(id))
//     }
//   }
// }


// export function listStatements() {
//   return {
//     type: LIST_STATEMENTS,
//     promise: fetchApiJson("statements", schemas.STATEMENT_ARRAY),
//   }
// }


// export function listStatementsIfNeeded() {
//   return (dispatch, getState) => {
//     const {statements} = getState()
//     return 
//     if (!statements || !statements.length) {
//       return dispatch(listStatements())
//     }
//   }
// }
