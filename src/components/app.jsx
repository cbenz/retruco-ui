// Retruco-UI -- Web user interface to bring out shared positions from argumented statements
// By: Paula Forteza <paula@retruco.org>
//     Emmanuel Raviart <emmanuel@retruco.org>
//
// Copyright (C) 2016 Paula Forteza & Emmanuel Raviart
// https://git.framasoft.org/retruco/retruco-ui
//
// Retruco-UI is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Retruco-UI is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {Component, PropTypes} from "react"
import {Link} from "react-router"
import {Container, NavItem, Toolbar} from "rebass"

import config from "../config"
import Breadcrumbs from "./breadcrumbs"


export default class App extends Component {
  static breadcrumbName = "Home"
  static propTypes = {
    children: PropTypes.node,
  }
  render() {
    const {children, params, routes} = this.props
    return (
      <div>
        <Toolbar>
          <NavItem is={Link} to="/">
            {config.title}
          </NavItem>
          <NavItem is={Link} to="/about">
            About
          </NavItem>
          <NavItem is={Link} to="/statements">
            Statements
          </NavItem>
        </Toolbar>
        <Container>
          <Breadcrumbs
            itemClassName={"btn button-narrow"}
            linkClassName={"btn button-narrow"}
            params={params}
            routes={routes}
            wrapperClassName={"mxn1"}
          />
          {children}
        </Container>
      </div>
    )
  }
}
