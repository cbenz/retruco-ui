// Retruco-UI -- Web user interface to bring out shared positions from argumented statements
// By: Paula Forteza <paula@retruco.org>
//     Emmanuel Raviart <emmanuel@retruco.org>
//
// Copyright (C) 2016 Paula Forteza & Emmanuel Raviart
// https://git.framasoft.org/retruco/retruco-ui
//
// Retruco-UI is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Retruco-UI is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import "isomorphic-fetch"
// import {normalize} from "normalizr"
import url from "url"

import config from "./config"


// Fetch helpers


function checkStatus(response) {
  if (response.ok) {
    return response
  } else {
    var error = new Error(response.statusText)
    error.response = response
    throw error
  }
}


// Fetchers


export function fetchApiJson(path, schema, fetchOptions) {
  // return fetch(url.resolve(config.apiBaseUrl, path), {method: "post"})
  const fetchUrl = url.resolve(config.apiBaseUrl, path)
  console.log(`Fetching ${fetchUrl}...`)
  return fetch(fetchUrl, fetchOptions)
    // .then(response => {
    //   if (! response.ok) return Promise.reject({
    //     error: response.statusText,
    //     response: response,
    //   })
    //   return response
    // })
    .then(checkStatus)
    .then(response => response.json())
    .then(json => json.data)
    // normalize(json, schema)
}


// Fetch data needed to render the view corresponding to a given route.
export function fetchRouteData(renderProps, store) {
  const {components} = renderProps
  const promises = components.reduce((memo, component) => {
    const fetchData = component.WrappedComponent ? // Is it a Redux "connected" component?
      component.WrappedComponent.fetchData :
      component.fetchData
    if (fetchData) {
      memo.push(fetchData(renderProps, store))
    }
    return memo
  }, [])
  return Promise.all(promises)
}
