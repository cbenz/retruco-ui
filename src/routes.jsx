// Retruco-UI -- Web user interface to bring out shared positions from argumented statements
// By: Paula Forteza <paula@retruco.org>
//     Emmanuel Raviart <emmanuel@retruco.org>
//
// Copyright (C) 2016 Paula Forteza & Emmanuel Raviart
// https://git.framasoft.org/retruco/retruco-ui
//
// Retruco-UI is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Retruco-UI is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {IndexRoute, Route} from "react-router"

import About from "./components/about"
import App from "./components/app"
import Home from "./components/home"
import NotFound from "./components/not-found"
import Statement from "./components/statement"
import Statements from "./components/statements"
import StatementsIndex from "./components/statements-index"


export default (
  <Route component={App} path="/">
    <IndexRoute component={Home} />
    <Route component={About} path="about" />
    <Route component={Statements} path="statements">
      <IndexRoute component={StatementsIndex} />
      <Route component={Statement} path=":id" />
    </Route>
    <Route component={NotFound} isNotFound path="*" />
  </Route>
)
