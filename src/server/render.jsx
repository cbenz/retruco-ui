// Retruco-UI -- Web user interface to bring out shared positions from argumented statements
// By: Paula Forteza <paula@retruco.org>
//     Emmanuel Raviart <emmanuel@retruco.org>
//
// Copyright (C) 2016 Paula Forteza & Emmanuel Raviart
// https://git.framasoft.org/retruco/retruco-ui
//
// Retruco-UI is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Retruco-UI is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {renderToStaticMarkup, renderToString} from "react-dom/server"
import {Provider} from "react-redux"
import {match, RouterContext} from "react-router"
import {syncReduxAndRouter} from "redux-simple-router"

// import {fetchRouteData} from "../fetchers"
import configureStore from "../store"
import routes from "../routes"
import rootSaga from "../sagas"
import HtmlDocument from "./html-document"


export default function handleRender(req, res, next) {
  const store = configureStore()
  match({routes, location: req.url}, (error, redirectLocation, renderProps) => {
    if (error) {
      // // TODO Disable in production?
      next(error)
      // res.status(500).send(error.message)
    } else if (redirectLocation) {
      res.redirect(302, redirectLocation.pathname + redirectLocation.search)
    // } else if (renderProps === null) {
    //   res.status(404).send("Not found")
    // } else {
    //   store.runSaga(rootSaga).done
    //     .then(() => res.send(renderHtmlDocument(renderProps, store)))
    //     .catch((error1) => next(error1))
    //   // fetchRouteData(renderProps, store)
    //   //   .then(() => res.send(renderHtmlDocument(renderProps, store)))
    //   //   .catch((error1) => next(error1))
    // }
    } else if (renderProps && renderProps.components) {
      // const rootComp = <Root store={store} routes={routes} history={createMemoryHistory()} renderProps={renderProps} type="server"/>


      store.runSaga(rootSaga).done.then(() => {
        res.send(renderHtmlDocument(renderProps, store))
        // res.status(200).send(
        //   layout(
        //     renderToString(rootComp),
        //     JSON.stringify(store.getState())
        //   )
        // )
      }).catch((e) => {
        console.log(e.message)
        next(Element)
        // res.status(500).send(e.message)
      })

      renderHtmlDocument(renderProps, store)
      // renderToString(rootComp)
      store.close()

      //res.status(200).send(layout('','{}'))
    } else {
      res.status(404).send("Not found")
    }
  })
}


function loadWebpackAssets() {
  const webpackAssetsFilePath = "../../webpack-assets.json"
  let webpackAssets
  if (process.env.NODE_ENV === "production") {
    webpackAssets = require(webpackAssetsFilePath)
  } else if (process.env.NODE_ENV === "development") {
    webpackAssets = require(webpackAssetsFilePath)
    // Do not cache webpack stats: the script file would change since
    // hot module replacement is enabled in the development env
    delete require.cache[require.resolve(webpackAssetsFilePath)]
  }
  return webpackAssets
}


function renderHtmlDocument(renderProps, store) {
  const appHtml = renderToString(
    <Provider store={store}>
      <RouterContext {...renderProps} />
    </Provider>
  )
  const webpackAssets = loadWebpackAssets()
  const html = renderToStaticMarkup(
    <HtmlDocument
      appHtml={appHtml}
      appInitialState={store.getState()}
      cssUrls={webpackAssets.main.css}
      jsUrls={webpackAssets.main.js}
    />
  )
  const doctype = "<!DOCTYPE html>"
  return doctype + html
}
