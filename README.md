# Retruco-UI

## Start

The first time only:

    npm install

To launch the web server:

    npm start

Then open http://localhost:3001/

## Production build

    npm run clean
    npm run build

Serve the contents of the `public` dir
(for example using [http-server](https://www.npmjs.com/package/http-server))):

    http-server public

Open `http://localhost:8080` in your browser.
