// Retruco-UI -- Web user interface to bring out shared positions from argumented statements
// By: Paula Forteza <paula@retruco.org>
//     Emmanuel Raviart <emmanuel@retruco.org>
//
// Copyright (C) 2016 Paula Forteza & Emmanuel Raviart
// https://git.framasoft.org/retruco/retruco-ui
//
// Retruco-UI is free statement; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Statement Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Retruco-UI is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {Component, PropTypes} from "react"
import {connect} from "react-redux"

import {loadStatement} from "../actions"
import StatementLine from "./statement-line"


class Statement extends Component {
  static propTypes = {
    loadStatement: PropTypes.func.isRequired,
    statementById: PropTypes.object.isRequired,
  }
  componentWillMount() {
    this.props.loadStatement(this.props.params.id)
  }
  render() {
    const {loadStatements, params, statementById} = this.props
    const statement = statementById[params.id]
    if (!statement) return (
      <p>Loading {params.id}...</p>
    )
    return (
      <section>
        <StatementLine statement={statement} />
        <button onClick={() => loadStatement(statement.id)}>Reload</button>
        <pre>{JSON.stringify(statement, null, 2)}</pre>
      </section>
    )
  }
}

export default connect(
  state => ({statementById: state.statementById}),
  {
    loadStatement,
  },
)(Statement)